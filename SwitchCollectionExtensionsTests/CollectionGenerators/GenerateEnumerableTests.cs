﻿using SwitchCollectionExtensions.CollectionGenerators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.CollectionGenerators
{
    [TestClass()]
    public class GenerateEnumerableTests
    {
        [TestMethod()]
        public void Test_IterateNullAction_EmptyCollectionReturned()
        {
            Assert.AreEqual(0, GenerateEnumerable.Iterate(1, null).Count());
        }

        [TestMethod()]
        public void Test_IterateValidAction_ReturnsExpectedCollection()
        {
            Func<int, int> doubleUp = (val) => 2 * val;

            Assert.IsTrue(
                new List<int>() { 1, 2, 4, 8, 16 }.Zip(
                    GenerateEnumerable.Iterate(1, doubleUp).Take(5),
                    (lhs, rhs) => lhs == rhs).All(compare => compare));
        }

        [TestMethod]
        public void Test_IterateFibonacci()
        {
            //decimal n = FibonacciGenerator(100);
            decimal n = FibonacciGenerator(10);

            Assert.AreEqual(55, n);
        }

        private decimal FibonacciGenerator(int nthFibo)
        {
            IEnumerable<decimal> fiboGen = new List<decimal>() { 1M, 1M }.Iterate(
                pair => new List<decimal>() { pair[1], pair[0] + pair[1] })
                .Skip(nthFibo - 1)
                .Take(1)
        .First();

            return fiboGen.ElementAt(0);
        }
    }
}