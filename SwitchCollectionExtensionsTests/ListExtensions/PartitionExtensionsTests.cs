﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using SwitchCollectionExtensions.ListExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.ListExtensions
{
    [TestClass]
    public class PartitionExtensionsTests
    {
        [TestMethod]
        public void TestPartitionBy_ListOfIntsEndsWithPartitionOfOneWithEqualsPartition_ListSplitByIntValue()
        {
            List<int> values = new List<int>() { 1, 1, 2, 1, 2, 2, 3, 3, 1 };
            IEnumerable<IEnumerable<int>> partitioned = values.PartitionBy((a) => a);

            Assert.AreEqual(6, partitioned.Count());
        }

        [TestMethod]
        public void TestPartitionBy_ListOfIntsEndsWithPartitionOfTwoWithEqualsPartition_ListSplitByIntValue()
        {
            List<int> values = new List<int>() { 1, 1, 2, 1, 2, 2, 3, 3 };
            IEnumerable<IEnumerable<int>> partitioned = values.PartitionBy((a) => a);

            Assert.AreEqual(5, partitioned.Count());
        }

        [TestMethod]
        public void TestPartitionBy_ListOfIntsWithLessThanPartition_ListSplitByIntValue()
        {
            List<int> values = new List<int>() { 1, 1, 2, 1, 2, 2, 3, 3 };
            IEnumerable<IEnumerable<int>> partitioned = values.PartitionBy((a) => a < 3);

            Assert.AreEqual(2, partitioned.Count());
        }

        [TestMethod]
        public void TestPartitionBy_ListOfStringsWithOneNull_ListSplitByStringValue()
        {
            List<string> values = new List<string>() { "1", "1", "2", null, "2", "2", "3", "3" };
            IEnumerable<IEnumerable<string>> partitioned = values.PartitionBy((a) => a);

            Assert.AreEqual(5, partitioned.Count());
        }

        [TestMethod]
        public void TestPartitionBy_ListOfStringsWithMultipleNulls_ListSplitByStringValue()
        {
            List<string> values = new List<string>() { "1", "1", "2", null, null, "2", "2", "3", "3" };
            IEnumerable<IEnumerable<string>> partitioned = values.PartitionBy((a) => a);

            Assert.AreEqual(5, partitioned.Count());
        }

        [TestMethod]
        public void TestPartitionBy_ListOfStringsWithSeparatedNulls_ListSplitByStringValue()
        {
            List<string> values = new List<string>() { "1", "1", "2", null, "2", null, "2", "3", "3" };
            IEnumerable<IEnumerable<string>> partitioned = values.PartitionBy((a) => a);

            Assert.AreEqual(7, partitioned.Count());
        }

        [TestMethod]
        public void TestPartitionBy_NullPartitionFunction_InputListReturned()
        {
            List<int> values = new List<int>() { 1, 1, 2, 1, 2, 2, 3, 3 };
            IEnumerable<List<int>> partitioned = values.PartitionBy<int, int>(null);

            Assert.AreEqual(1, partitioned.Count());

            values.Zip(partitioned.First().ToList()).All(pair => pair.First() == pair.Last());
        }

        [TestMethod]
        public void TestPartitionBy_EmptyInput_InputListReturned()
        {
            List<int> values = new List<int>();
            IEnumerable<List<int>> partitioned = values.PartitionBy<int, int>(null);

            Assert.AreEqual(1, partitioned.Count());
            Assert.AreEqual(0, partitioned.First().Count);
        }
    }
}
