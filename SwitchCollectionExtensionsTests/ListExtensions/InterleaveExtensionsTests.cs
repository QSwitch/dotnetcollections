﻿using SwitchCollectionExtensions.ListExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.ListExtensions
{
    [TestClass]
    public class InterleaveExtensionsTests
    {
        private List<int> _lhsValues = new List<int>() { 1, 2, 3, 4, 5 };
        private List<int> _rhsValues = new List<int>() { 1, 2, 3, 4, 5 };
        private List<int> _expectedEqualList = new List<int>() { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5 };

        [TestMethod]
        public void Test_EmptyLists_EmptyResult()
        {
            List<int> empty = new List<int>();
            List<int> result = empty.Interleave(empty).ToList();

            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void Test_NullRhs_LhsReturned()
        {
            List<int> result = _lhsValues.Interleave(null).ToList();

            Assert.AreEqual(_lhsValues.Count, result.Count);

            foreach (int index in Enumerable.Range(0, _lhsValues.Count))
            {
                Assert.AreEqual(_lhsValues[index], result[index]);
            }
        }

        [TestMethod]
        public void Test_EmptyLhs_RhsReturned()
        {
            List<int> result = new List<int>().Interleave(_lhsValues).ToList();

            Assert.AreEqual(_lhsValues.Count, result.Count);

            foreach (int index in Enumerable.Range(0, _lhsValues.Count))
            {
                Assert.AreEqual(_lhsValues[index], result[index]);
            }
        }

        [TestMethod]
        public void Test_InterleaveEqualSizeLists_EntriesInterleaved()
        {
            List<int> result = _lhsValues.Interleave(_rhsValues).ToList();

            Assert.AreEqual(_expectedEqualList.Count, result.Count);

            foreach (int index in Enumerable.Range(0, _expectedEqualList.Count))
            {
                Assert.AreEqual(_expectedEqualList[index], result[index]);
            }
        }

        [TestMethod]
        public void Test_InterleaveRhsShorter_AdditionalLhsEntriesAtEnd()
        {
            List<int> result = _lhsValues.Interleave(_rhsValues.Skip(1).ToList()).ToList();
            List<int> expectedResult = _expectedEqualList.Skip(1).ToList();

            Assert.AreEqual(expectedResult.Count, result.Count);

            foreach (int index in Enumerable.Range(0, expectedResult.Count))
            {
                Assert.AreEqual(expectedResult[index], result[index]);
            }
        }

        [TestMethod]
        public void Test_InterleaveLhsShorter_AdditionalRhsEntriesAtEnd()
        {
            List<int> result = _lhsValues.Skip(1).ToList().Interleave(_rhsValues).ToList();
            List<int> expectedResult = new List<int>() { 2, 1, 3, 2, 4, 3, 5, 4, 5 };

            Assert.AreEqual(expectedResult.Count, result.Count);

            foreach (int index in Enumerable.Range(0, expectedResult.Count))
            {
                Assert.AreEqual(expectedResult[index], result[index]);
            }
        }

        [TestMethod]
        public void Test_InterleaveSingleVal_ValBetweenTargetEntries()
        {
            int valueToInterleave = 10;
            List<int> result = _lhsValues.Interleave(valueToInterleave).ToList();
            int expectedCount = 2 * _lhsValues.Count - 1;

            Assert.AreEqual(expectedCount, result.Count);

            foreach (int index in Enumerable.Range(0, expectedCount))
            {
                if ((index % 2) == 0)
                {
                    Assert.AreEqual(_lhsValues[index / 2], result[index]);
                }
                else
                {
                    Assert.AreEqual(valueToInterleave, result[index]);
                }
            }
        }

        [TestMethod]
        public void Test_InterleaveSingleValInEmptyList_ReturnsEmpty()
        {
            int valueToInterleave = 10;
            List<int> result = new List<int>().Interleave(valueToInterleave).ToList();

            Assert.AreEqual(0, result.Count);
        }
    }
}