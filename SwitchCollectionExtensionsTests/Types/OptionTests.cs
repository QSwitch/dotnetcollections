﻿using SwitchCollectionExtensions.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SwitchCollectionExtensions.TypesTests
{
    [TestClass()]
    public class OptionTests
    {
        [TestMethod()]
        public void TestHasValue_DefaultReferenceOption_NoValue()
        {
            Assert.IsFalse(new Option<string>(null).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_DefaultValueOption_NoValue()
        {
            Assert.IsFalse(new Option<int>(default(int)).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NonNullRefDefaultPredicate_HasValue()
        {
            Assert.IsTrue(new Option<string>("test").HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NullRefCustomPredicateThatMatches_NoValue()
        {
            Assert.IsFalse(new Option<string>(null, (val) => val == null).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NullRefCustomPredicateThatDoesNotMatch_NoValue()
        {
            Assert.IsTrue(new Option<string>(null, (val) => val != null).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NonNullRefCustomPredicateThatMatches_NoValue()
        {
            Assert.IsFalse(new Option<string>("test", (val) => val == "test").HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NonNullRefCustomPredicateThatDoesNotMatch_HasValue()
        {
            Assert.IsTrue(new Option<string>("test", (val) => val == "testing").HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NonDefaultIntDefaultPredicate_HasValue()
        {
            Assert.IsTrue(new Option<int>(1).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_DefaultValueCustomPredicateThatMatches_NoValue()
        {
            Assert.IsFalse(new Option<int>(default(int), (val) => val == 0).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_DefaultValueCustomPredicateThatDoesNotMatch_NoValue()
        {
            Assert.IsTrue(new Option<int>(default(int), (val) => val != 0).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NonDefaultValueCustomPredicateThatMatches_NoValue()
        {
            Assert.IsFalse(new Option<int>(1, (val) => val > 0).HasValue);
        }

        [TestMethod()]
        public void TestHasValue_NonDefaultValueCustomPredicateThatDoesNotMatch_HasValue()
        {
            Assert.IsTrue(new Option<int>(1, (val) => val <= 0).HasValue);
        }
    }
}