﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.IEnumerableExtensions
{
    [TestClass]
    public class ZipExtensionsWithHashSetTests
    {
        private HashSet<int> _testValues = new HashSet<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        [TestMethod]
        public void TestZip_EqualHashSets_ReturnsPairsByKey()
        {
            HashSet<int> rhs = _testValues.Select(val => val * 10).ToHashSet();
            IEnumerable<IEnumerable<int>> zipped = _testValues.Zip(rhs);

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());
                    Assert.AreEqual(pair.ElementAt(1), pair.ElementAt(0) * 10);
                });
        }
    }
}
