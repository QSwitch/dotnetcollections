﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.IEnumerableExtensions
{
    [TestClass]
    public class AppendExtensionsWithListTests
    {
        private List<int> _testValues = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        [TestMethod]
        public void TestAppend_ThreeCollections_ResultIsAllThreeAppended()
        {
            int entriesToTake = 5;
            int firstMultiplier = 10;
            int secondMultiplier = 20;
            List<int> second = _testValues.Take(entriesToTake).Select(val => val * firstMultiplier).ToList();
            List<int> third = _testValues.Take(entriesToTake).Select(val => val * secondMultiplier).ToList();
            List<int> result = _testValues.Append(second, third).ToList();

            Assert.AreEqual(_testValues.Count + 2 * entriesToTake, result.Count);

            IEnumerable<int> firstPart = result.Take(_testValues.Count);
            IEnumerable<int> secondPart = result.Skip(_testValues.Count).Take(entriesToTake);
            IEnumerable<int> thirdPart = result.Skip(_testValues.Count + entriesToTake);

            _testValues.Zip(firstPart).ForEach(pair => Assert.AreEqual(pair.First(), pair.Last()));
            _testValues.Zip(secondPart).ForEach(pair => Assert.AreEqual(pair.First() * firstMultiplier, pair.Last()));
            _testValues.Zip(thirdPart).ForEach(pair => Assert.AreEqual(pair.First() * secondMultiplier, pair.Last()));
        }

        [TestMethod]
        public void TestAppend_ThirdCollectionIsNull_ResultIsAllThreeAppended()
        {
            int entriesToTake = 5;
            int firstMultiplier = 10;
            List<int> second = _testValues.Take(entriesToTake).Select(val => val * firstMultiplier).ToList();
            List<int> result = _testValues.Append(second, null).ToList();

            Assert.AreEqual(_testValues.Count + entriesToTake, result.Count);

            IEnumerable<int> firstPart = result.Take(_testValues.Count);
            IEnumerable<int> secondPart = result.Skip(_testValues.Count).Take(entriesToTake);

            _testValues.Zip(firstPart).ForEach(pair => Assert.AreEqual(pair.First(), pair.Last()));
            _testValues.Zip(secondPart).ForEach(pair => Assert.AreEqual(pair.First() * firstMultiplier, pair.Last()));
        }

        [TestMethod]
        public void TestAppend_OneNullExtraCollection_ResultIsValidThreeAppended()
        {
            int entriesToTake = 5;
            int firstMultiplier = 10;
            int secondMultiplier = 20;
            List<int> second = _testValues.Take(entriesToTake).Select(val => val * firstMultiplier).ToList();
            List<int> third = _testValues.Take(entriesToTake).Select(val => val * secondMultiplier).ToList();
            List<int> result = _testValues.Append(second, null, third).ToList();

            Assert.AreEqual(_testValues.Count + 2 * entriesToTake, result.Count);

            IEnumerable<int> firstPart = result.Take(_testValues.Count);
            IEnumerable<int> secondPart = result.Skip(_testValues.Count).Take(entriesToTake);
            IEnumerable<int> thirdPart = result.Skip(_testValues.Count + entriesToTake);

            _testValues.Zip(firstPart).ForEach(pair => Assert.AreEqual(pair.First(), pair.Last()));
            _testValues.Zip(secondPart).ForEach(pair => Assert.AreEqual(pair.First() * firstMultiplier, pair.Last()));
            _testValues.Zip(thirdPart).ForEach(pair => Assert.AreEqual(pair.First() * secondMultiplier, pair.Last()));
        }
    }
}
