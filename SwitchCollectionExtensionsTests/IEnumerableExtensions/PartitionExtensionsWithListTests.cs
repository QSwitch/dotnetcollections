﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.IEnumerableExtensions
{
    [TestClass()]
    public class PartitionExtensionsWithListTests
    {
        private List<int> _testValues = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        [TestMethod()]
        public void TestPartition_ZeroSize_ReturnsWholeCollection()
        {
            Assert.AreEqual(_testValues, _testValues.Partition(0).First());
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsWholeCollection()
        {
            Assert.AreEqual(_testValues, _testValues.Partition(-1).First());
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsOnlyCollection()
        {
            Assert.AreEqual(1, _testValues.Partition(-1).Take(2).Count());
        }

        [TestMethod()]
        public void TestPartition_SingleSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(1);
        }

        private void AssertEvenDivisionPartitionResults(int partitionSize)
        {
            IEnumerable<IEnumerable<int>> partitioned = _testValues.Partition(partitionSize);

            Assert.AreEqual(_testValues.Count / partitionSize, partitioned.Count());

            foreach (IEnumerable<int> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        [TestMethod()]
        public void TestPartition_ExactDivisorSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(2);
        }

        [TestMethod()]
        public void TestPartition_UnevenDivisorSize_ReturnsOnlyFullSizeCollections()
        {
            int partitionSize = 3;
            IEnumerable<IEnumerable<int>> partitioned = _testValues.Partition(partitionSize);
            int expectedCount = _testValues.Count / partitionSize;

            Assert.AreEqual(expectedCount, partitioned.Count());

            foreach (IEnumerable<int> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        [TestMethod()]
        public void TestPartitionWithPad_UnevenDivisorSize_ReturnsPaddedLastCollection()
        {
            int partitionSize = 3;
            int pad = -1;
            IEnumerable<IEnumerable<int>> partitioned = _testValues.Partition(partitionSize, partitionSize, pad);
            int expectedCount = 1 + _testValues.Count / partitionSize;

            Assert.AreEqual(expectedCount, partitioned.Count());

            foreach (IEnumerable<int> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }

            Assert.AreEqual(_testValues.Count % partitionSize, partitioned.Last().TakeWhile(val => val != pad).Count());
        }

        [TestMethod]
        public void Test_EveryOtherItem_ReturnsOddPositionItems()
        {
            List<int> result = _testValues.EveryNth(2).ToList();
            int expectedCount = _testValues.Count / 2;
            List<int> expectedResult = new List<int>() { 1, 3, 5, 7, 9 };

            Assert.AreEqual(expectedCount, result.Count);

            foreach (int index in Enumerable.Range(0, expectedCount))
            {
                Assert.AreEqual(expectedResult[index], result[index]);
            }
        }

        [TestMethod]
        public void TestPartition_WithZeroStep_StepSetTo1()
        {
            int partitionSize = 3;
            IEnumerable<IEnumerable<int>> partitions = _testValues.Partition(partitionSize, 0);

            AssertPartitionWithStepResults(partitions, _testValues.Count, partitionSize, 1);
        }

        private void AssertPartitionWithStepResults(
            IEnumerable<IEnumerable<int>> partitions,
            int inputCollCount,
            int partitionSize,
            int stepSize)
        {
            int startVal = 1;
            IEnumerator<IEnumerable<int>> partitionEnumerator = partitions.GetEnumerator();

            while (partitionEnumerator.MoveNext())
            {
                List<int> result = partitionEnumerator.Current.ToList();
                List<int> expected = Enumerable.Range(startVal + (startVal - 1) * (stepSize - 1), result.Count).ToList();
                List<List<int>> zipped = expected.Zip(result).ToList();

                zipped.ForEach(pair => Assert.AreEqual(pair.ElementAt(0), pair.ElementAt(1)));
                ++startVal;
            }

            int expectedPartitionCount = GetExpectedPartitionCount(inputCollCount, partitionSize, stepSize);

            Assert.AreEqual(expectedPartitionCount, startVal - 1);
        }

        private int GetExpectedPartitionCount(int collSize, int partitionSize, int stepSize)
        {
            int partitionCount = 0;

            while (1 + (partitionCount * stepSize) + partitionSize - 1 <= collSize)
            {
                ++partitionCount;
            }

            return partitionCount;
        }

        [TestMethod]
        public void TestPartition_WithStepLessThanPartition_OverlappingResults()
        {
            int partitionSize = 3;
            int stepSize = 2;
            IEnumerable<IEnumerable<int>> partitions = _testValues.Partition(partitionSize, stepSize);

            AssertPartitionWithStepResults(partitions, _testValues.Count, partitionSize, stepSize);
        }

        [TestMethod]
        public void TestPartition_WithStepAndPartitionSizeGreaterThanInputSize_Empty()
        {
            int partitionSize = _testValues.Count + 1;
            int stepSize = 2;
            IEnumerable<IEnumerable<int>> partitions = _testValues.Partition(partitionSize, stepSize);

            AssertPartitionWithStepResults(partitions, _testValues.Count, partitionSize, stepSize);
        }
    }
}