﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.IEnumerableExtensions
{
    [TestClass]
    public class SelectWhereTests
    {
        private class Dummy
        {
            public int Index { get; set; }
            public string Name { get; set; }
        }

        [TestMethod]
        public void TestSelectWhere_EmptyCollection_EmptyResult()
        {
            IEnumerable<int> result = new List<Dummy>().SelectWhere(dummy => dummy.Index > 5, dummy => dummy.Index);
            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void TestSelectWhere_SingleMatchingItem_SingleResult()
        {
            int valueToInsert = 6;
            List<Dummy> dummies = new List<Dummy>()
        {
                new Dummy()
        {
            Index = valueToInsert, Name = "name"
            }
            };
            IEnumerable<int> result = dummies.SelectWhere(dummy => dummy.Index >= valueToInsert, dummy => dummy.Index);

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void TestSelectWhere_SingleNonMatchingItem_EmptyResult()
        {
            int valueToInsert = 4;
            List<Dummy> dummies = new List<Dummy>()
        {
                new Dummy()
        {
            Index = valueToInsert, Name = "name"
            }
            };
            IEnumerable<int> result = dummies.SelectWhere(dummy => dummy.Index > valueToInsert, dummy => dummy.Index);

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void TestSelectWhere_MultipleMatches_MultipleResults()
        {
            Tuple<int, List<Dummy>> midIndexAndDummies = GenerateDummies();
            int midIndex = midIndexAndDummies.Item1;
            List<Dummy> dummies = midIndexAndDummies.Item2;
            List<int> indexesGreaterThanMid =
                dummies.Select(dummy => dummy.Index).Where(index => index > midIndex).ToList();
            List<int> dummyIndexes = dummies.SelectWhere(dummy => dummy.Index > midIndex, dummy => dummy.Index).ToList();

            indexesGreaterThanMid
                .Zip(dummyIndexes)
                .ForEach(indexPair => Assert.AreEqual(indexPair.First(), indexPair.Last()));
        }

        private Tuple<int, List<Dummy>> GenerateDummies()
        {
            Random generator = new Random();
            List<Dummy> dummies = new List<Dummy>();
            int minIndex = Int32.MaxValue;
            int maxIndex = Int32.MinValue;
            Dictionary<int, int> indexEncounteredCount = new Dictionary<int, int>();

            Enumerable.Range(0, generator.Next(10, 100))
                .ForEach(index =>
            {
                if (!indexEncounteredCount.Keys.Contains(index))
                {
                    indexEncounteredCount.Add(index, 1);
                }
                else
                {
                    indexEncounteredCount[index] += 1;
                }

                string dummyName = indexEncounteredCount[index].ToString();
                dummies.Add(new Dummy() { Index = index, Name = dummyName });

                minIndex = Math.Min(minIndex, index);
                maxIndex = Math.Max(maxIndex, index);
            });

            return new Tuple<int, List<Dummy>>((int)Math.Floor((minIndex + maxIndex) / 2M), dummies);
        }


        [TestMethod]
        public void TestSelectDistinctWhere_EmptyCollection_EmptyResult()
        {
            IEnumerable<int> result =
        new List<Dummy>().SelectDistinctWhere(dummy => dummy.Index > 5, dummy => dummy.Index);
            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void TestSelectDistinctWhere_SingleMatchingItem_SingleResult()
        {
            int valueToInsert = 6;
            List<Dummy> dummies = new List<Dummy>()
            {
                new Dummy()
                {
                    Index = valueToInsert, Name = "name"
                }
            };
            IEnumerable<int> result =
        dummies.SelectDistinctWhere(dummy => dummy.Index >= valueToInsert, dummy => dummy.Index);

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void TestSelectDistinctWhere_DuplicateMatchingItems_SingleResult()
        {
            int valueToInsert = 6;
            List<Dummy> dummies = new List<Dummy>()
            {
                new Dummy()
                {
                    Index = valueToInsert, Name = "name"
                },
                new Dummy()
                {
                    Index = valueToInsert, Name = "name"
                }
            };
            IEnumerable<int> result =
        dummies.SelectDistinctWhere(dummy => dummy.Index >= valueToInsert, dummy => dummy.Index);

            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void TestSelectDistinctWhere_SingleNonMatchingItem_EmptyResult()
        {
            int valueToInsert = 4;
            List<Dummy> dummies = new List<Dummy>()
            {
                new Dummy()
                {
                    Index = valueToInsert, Name = "name"
                }
            };
            IEnumerable<int> result =
                dummies.SelectDistinctWhere(dummy => dummy.Index > valueToInsert, dummy => dummy.Index);

            Assert.AreEqual(0, result.Count());
        }

        [TestMethod]
        public void TestSelectDistinctWhere_MultipleMatches_OnlyDistinctIntResults()
        {
            Tuple<int, List<Dummy>> midIndexAndDummies = GenerateDummies();
            int midIndex = midIndexAndDummies.Item1;
            List<Dummy> dummies = midIndexAndDummies.Item2;
            List<int> indexesGreaterThanMid =
                dummies.Select(dummy => dummy.Index).Distinct().Where(index => index > midIndex).ToList();
            List<int> dummyIndexes =
                dummies.SelectDistinctWhere(dummy => dummy.Index > midIndex, dummy => dummy.Index).ToList();

            indexesGreaterThanMid
                .Zip(dummyIndexes)
                .ForEach(indexPair => Assert.AreEqual(indexPair.First(), indexPair.Last()));
        }

        [TestMethod]
        public void TestSelectDistinctWhere_MultipleMatches_OnlyDistinctStringResults()
        {
            Tuple<int, List<Dummy>> midIndexAndDummies = GenerateDummies();
            int midIndex = midIndexAndDummies.Item1;
            List<Dummy> dummies = midIndexAndDummies.Item2;
            List<string> namesWhereIndexGreaterThanMid =
                dummies.Where(dummy => dummy.Index > midIndex).Select(dummy => dummy.Name).Distinct().ToList();
            List<string> dummyIndexes =
                dummies.SelectDistinctWhere(dummy => dummy.Index > midIndex, dummy => dummy.Name).ToList();

            namesWhereIndexGreaterThanMid
                .Zip(dummyIndexes)
                .ForEach(indexPair =>
                {
                    Assert.AreEqual(indexPair.First(), indexPair.Last());
                    Assert.AreEqual("1", indexPair.Last());
                });
        }
    }
}
