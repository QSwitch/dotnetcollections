﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.IEnumerableExtensions
{
    [TestClass]
    public class PartitionExtensionsWithDictionaryTests
    {
        private Dictionary<string, int> _testValues = new Dictionary<string, int>()
        {
            { "1", 1},
            { "2", 2},
            { "3", 3},
            { "4", 4},
            { "5", 5},
            { "6", 6},
            { "7", 7},
            { "8", 8},
            { "9", 9},
            { "10", 10},
        };

        [TestMethod()]
        public void TestPartition_ZeroSize_ReturnsWholeCollection()
        {
            Assert.AreEqual(_testValues, _testValues.Partition(0).First());
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsWholeCollection()
        {
            Assert.AreEqual(_testValues, _testValues.Partition(-1).First());
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsOnlyCollection()
        {
            Assert.AreEqual(1, _testValues.Partition(-1).Take(2).Count());
        }

        [TestMethod()]
        public void TestPartition_SingleSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(1);
        }

        private void AssertEvenDivisionPartitionResults(int partitionSize)
        {
            IEnumerable<IEnumerable<KeyValuePair<string, int>>> partitioned = _testValues.Partition(partitionSize);

            Assert.AreEqual(_testValues.Count / partitionSize, partitioned.Count());

            foreach (IEnumerable<KeyValuePair<string, int>> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        [TestMethod()]
        public void TestPartition_ExactDivisorSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(2);
        }

        [TestMethod()]
        public void TestPartition_UnevenDivisorSize_ReturnsOnlyFullCollections()
        {
            int partitionSize = 3;
            IEnumerable<IEnumerable<KeyValuePair<string, int>>> partitioned = _testValues.Partition(partitionSize);
            int expectedCount = _testValues.Count / partitionSize;

            Assert.AreEqual(expectedCount, partitioned.Count());

            foreach (IEnumerable<KeyValuePair<string, int>> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        [TestMethod()]
        public void TestPartition_UnevenDivisorSize_ReturnsPartialLastCollection()
        {
            int partitionSize = 3;
            KeyValuePair<string, int> pad = new KeyValuePair<string, int>("-1", -1);
            IEnumerable<IEnumerable<KeyValuePair<string, int>>> partitioned = _testValues.Partition(
                partitionSize,
                partitionSize,
                pad);
            int expectedCount = 1 + _testValues.Count / partitionSize;

            Assert.AreEqual(expectedCount, partitioned.Count());

            foreach (IEnumerable<KeyValuePair<string, int>> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }

            Assert.AreEqual(
                _testValues.Count % partitionSize,
                partitioned.Last().TakeWhile(pair => pair.Key != pad.Key && pair.Value != pad.Value).Count());
        }
    }
}
