﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.IEnumerableExtensions
{
    [TestClass()]
    public class PartitionExtensionsWithHashSetTests
    {
        private HashSet<int> _testValues = new HashSet<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        [TestMethod()]
        public void TestPartition_ZeroSize_ReturnsWholeCollection()
        {
            Assert.AreEqual(_testValues, _testValues.Partition(0).First());
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsWholeCollection()
        {
            Assert.AreEqual(_testValues, _testValues.Partition(-1).First());
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsOnlyCollection()
        {
            Assert.AreEqual(1, _testValues.Partition(-1).Take(2).Count());
        }

        [TestMethod()]
        public void TestPartition_SingleSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(1);
        }

        private void AssertEvenDivisionPartitionResults(int partitionSize)
        {
            IEnumerable<IEnumerable<int>> partitioned = _testValues.Partition(partitionSize);

            Assert.AreEqual(_testValues.Count / partitionSize, partitioned.Count());

            foreach (IEnumerable<int> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        [TestMethod()]
        public void TestPartition_ExactDivisorSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(2);
        }

        [TestMethod()]
        public void TestPartition_UnevenDivisorSize_ReturnsOnlyFullSizeCollections()
        {
            int partitionSize = 3;
            IEnumerable<IEnumerable<int>> partitioned = _testValues.Partition(partitionSize);
            int expectedCount = 1 + _testValues.Count / partitionSize;

            Assert.AreEqual(expectedCount - 1, partitioned.Count());

            foreach (IEnumerable<int> partition in partitioned.Take(expectedCount - 1))
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        [TestMethod()]
        public void TestPartitionWithPad_UnevenDivisorSize_ReturnsPaddedLastCollection()
        {
            int partitionSize = 3;
            int pad = -1;
            IEnumerable<IEnumerable<int>> partitioned = _testValues.Partition(partitionSize, partitionSize, pad);
            int expectedCount = 1 + _testValues.Count / partitionSize;

            Assert.AreEqual(expectedCount, partitioned.Count());

            foreach (IEnumerable<int> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }

            Assert.AreEqual(_testValues.Count % partitionSize, partitioned.Last().TakeWhile(val => val != pad).Count());
        }
    }
}