﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.IEnumerableExtensions
{
    [TestClass]
    public class ZipExtensionsWithListTests
    {
        private List<int> _testValues = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        [TestMethod]
        public void TestZip_EqualLists_ReturnsPairsByIndex()
        {
            List<int> rhs = _testValues.Select(val => val * 10).ToList();
            IEnumerable<IEnumerable<int>> zipped = _testValues.Zip(rhs);

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());
                    Assert.AreEqual(pair.ElementAt(1), pair.ElementAt(0) * 10);
                });
        }

        [TestMethod]
        public void TestZip_EmptyRhs_ReturnsEmptyCollection()
        {
            IEnumerable<IEnumerable<int>> zipped = _testValues.Zip(new List<int>());

            AssertEmptyZipResult(zipped);
        }

        private void AssertEmptyZipResult<T>(IEnumerable<IEnumerable<T>> zipped)
        {
            Assert.AreEqual(1, zipped.Count());
            Assert.AreEqual(false, zipped.ElementAt(0).Any());
        }

        [TestMethod]
        public void TestZip_NullRhs_ReturnsEmptyCollection()
        {
            IEnumerable<IEnumerable<int>> zipped = _testValues.Zip(null);

            AssertEmptyZipResult(zipped);
        }

        [TestMethod]
        public void TestZip_LhsLonger_ReturnsPairsByIndexWithOneFewerEntry()
        {
            int entriesToTake = 4;
            List<int> rhs = _testValues.Take(entriesToTake).Select(val => val * 10).ToList();
            IEnumerable<IEnumerable<int>> zipped = _testValues.Zip(rhs);

            Assert.AreEqual(entriesToTake, zipped.Count());

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());
                    Assert.AreEqual(pair.ElementAt(1), pair.ElementAt(0) * 10);
                });
        }

        [TestMethod]
        public void TestZip_RhsLonger_ReturnsPairsByIndexExceptForLastEntry()
        {
            List<int> lhs = _testValues.Skip(1).Select(val => val * 10).ToList();
            IEnumerable<IEnumerable<int>> zipped = _testValues.Zip(lhs);

            Assert.AreEqual(lhs.Count, zipped.Count());

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());
                    Assert.AreEqual((pair.ElementAt(0) + 1) * 10, pair.ElementAt(1));
                });
        }

        [TestMethod]
        public void TestZip_EmptyLhs_ReturnsEmptyCollection()
        {
            IEnumerable<IEnumerable<int>> zipped = new List<int>().Zip(_testValues);

            AssertEmptyZipResult(zipped);
        }

        [TestMethod]
        public void TestFlatten_PartitionedCollection_ResultMatchesInput()
        {
            IEnumerable<int> result = _testValues.Partition(2).Flatten<IEnumerable<int>, int>();
            IEnumerable<IEnumerable<int>> zippedResult = _testValues.Zip(result.ToList());

            zippedResult.ForEach(
                expectedAndResult => Assert.AreEqual(expectedAndResult.ElementAt(0), expectedAndResult.ElementAt(1)));
        }

        [TestMethod]
        public void TestZipAny_EmptyRhs_ReturnsLhsPairedWithDefault()
        {
            IEnumerable<IEnumerable<int>> zipped = _testValues.ZipAny(new List<int>());

            int val = _testValues.First();

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());
                    Assert.AreEqual(val, pair.First());
                    Assert.AreEqual(default(int), pair.Last());
                    ++val;
                });
        }

        [TestMethod]
        public void TestZipAny_NullRhs_ReturnsLhsEquivalent()
        {
            IEnumerable<IEnumerable<int>> zipped = _testValues.ZipAny(null);

            int val = _testValues.First();

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(1, pair.Count());
                    Assert.AreEqual(val, pair.First());
                    ++val;
                });
        }

        [TestMethod]
        public void TestZipAny_EmptyLhs_ReturnsDefaultPairedWithRhs()
        {
            IEnumerable<IEnumerable<int>> zipped = new List<int>().ZipAny(_testValues);

            int val = _testValues.First();

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());
                    Assert.AreEqual(default(int), pair.First());
                    Assert.AreEqual(val, pair.Last());
                    ++val;
                });
        }

        [TestMethod]
        public void TestZipAny_LhsLonger_ReturnsPairsByIndexExceptForLastEntry()
        {
            int entriesToTake = 4;
            List<int> rhs = _testValues.Take(entriesToTake).Select(val => val * 10).ToList();
            IEnumerable<IEnumerable<int>> zipped = _testValues.ZipAny(rhs);
            int index = 0;

            Assert.AreEqual(_testValues.Count, zipped.Count());

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());

                    if (index < entriesToTake)
                    {
                        Assert.AreEqual(pair.ElementAt(1), pair.ElementAt(0) * 10);
                    }
                    else
                    {
                        Assert.AreEqual(_testValues[index], pair.ElementAt(0));
                        Assert.AreEqual(default(int), pair.ElementAt(1));
                    }

                    ++index;
                });
        }

        [TestMethod]
        public void TestZipAny_RhsLonger_ReturnsPairsByIndexExceptForLastEntry()
        {
            List<int> lhs = _testValues.Skip(1).Select(val => val * 10).ToList();
            IEnumerable<IEnumerable<int>> zipped = lhs.ZipAny(_testValues);
            int index = 0;

            zipped.ToList().ForEach(
                pair =>
                {
                    Assert.AreEqual(2, pair.Count());

                    if (index < lhs.Count)
                    {
                        Assert.AreEqual(pair.ElementAt(0), (pair.ElementAt(1) + 1) * 10);
                    }
                    else
                    {
                        Assert.AreEqual(default(int), pair.ElementAt(0));
                        Assert.AreEqual(_testValues.Last(), pair.ElementAt(1));
                    }

                    ++index;
                });
        }
    }
}
