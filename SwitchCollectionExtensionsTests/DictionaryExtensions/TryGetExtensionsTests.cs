﻿using SwitchCollectionExtensions.DictionaryExtensions;
using SwitchCollectionExtensions.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SwitchCollectionExtensions.DictionaryExtensionsTests
{
    [TestClass()]
    public class TryGetExtensionsTests
    {
        private const string KeyPresent = "test1";
        private const string KeyNotPresent = "test11";
        private const int PresentValue = 1;

        private Dictionary<string, string> _dictWithRefs = new Dictionary<string, string>()
        {
            { KeyPresent, KeyPresent }
        };

        private Dictionary<string, int> _dictWithVals = new Dictionary<string, int>()
        {
            { KeyPresent, PresentValue }
        };

        [TestMethod()]
        public void TestTryGetRef_KeyPresent_OptionHasValue()
        {
            Option<string> dictVal = _dictWithRefs.TryGet(KeyPresent);

            Assert.IsTrue(dictVal.HasValue);
            Assert.AreEqual(KeyPresent, dictVal.Value);
        }

        [TestMethod()]
        public void TestTryGetVal_KeyPresent_OptionHasValue()
        {
            Option<int> dictVal = _dictWithVals.TryGet(KeyPresent);

            Assert.IsTrue(dictVal.HasValue);
            Assert.AreEqual(PresentValue, dictVal.Value);
        }

        [TestMethod()]
        public void TestTryGetRef_KeyNotPresent_NoDefault_OptionHasNoValue()
        {
            Option<string> dictVal = _dictWithRefs.TryGet(KeyNotPresent);

            Assert.IsFalse(dictVal.HasValue);
        }

        [TestMethod()]
        public void TestTryGetVal_KeyNotPresent_NoDefault_OptionHasNoValue()
        {
            Option<int> dictVal = _dictWithVals.TryGet(KeyNotPresent);

            Assert.IsFalse(dictVal.HasValue);
        }

        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestTryGetRef_KeyNotPresent_NoDefault_ValuePropertyThrowsException()
        {
            Option<string> dictVal = _dictWithRefs.TryGet(KeyNotPresent);

            Assert.IsFalse(dictVal.HasValue);
            string result = dictVal.Value;
        }

        [TestMethod()]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestTryGetVal_KeyNotPresent_NoDefault_ValuePropertyThrowsException()
        {
            Option<int> dictVal = _dictWithVals.TryGet(KeyNotPresent);

            Assert.IsFalse(dictVal.HasValue);
            int result = dictVal.Value;
        }

        [TestMethod()]
        public void TestTryGetRef_KeyNotPresent_ValidDefault_OptionHasDefaultValue()
        {
            string defaultValue = "Not found";
            Option<string> dictVal = _dictWithRefs.TryGet(KeyNotPresent, defaultValue);

            Assert.IsTrue(dictVal.HasValue);
            Assert.AreEqual(defaultValue, dictVal.Value);
        }

        [TestMethod()]
        public void TestTryGetRef_KeyNotPresent_NullDefault_OptionHasNoValue()
        {
            Option<string> dictVal = _dictWithRefs.TryGet(KeyNotPresent, null);

            Assert.IsFalse(dictVal.HasValue);
        }

        [TestMethod()]
        public void TestTryGetVal_KeyNotPresent_ValidDefault_OptionHasDefaultValue()
        {
            int defaultValue = -100;
            Option<int> dictVal = _dictWithVals.TryGet(KeyNotPresent, defaultValue);

            Assert.IsTrue(dictVal.HasValue);
            Assert.AreEqual(defaultValue, dictVal.Value);
        }
    }
}