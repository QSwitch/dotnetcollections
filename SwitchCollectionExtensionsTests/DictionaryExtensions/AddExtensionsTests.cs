﻿using SwitchCollectionExtensions.DictionaryExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace SwitchCollectionExtensions.DictionaryExtensionsTests
{
    [TestClass()]
    public class AddExtensionsTests
    {
        [TestMethod()]
        public void TestTryAdd_KeyNotPresent_Succeeds()
        {
            string keyPresent = "test1";
            string keyNotPresent = keyPresent + "1";
            Dictionary<string, int> vals = new Dictionary<string, int>() { { keyPresent, 1 } };

            Assert.IsTrue(vals.TryAdd(keyNotPresent, 2));
        }

        [TestMethod()]
        public void TestTryAdd_KeyPresent_Fails()
        {
            string keyPresent = "test1";
            Dictionary<string, int> vals = new Dictionary<string, int>() { { keyPresent, 1 } };

            Assert.IsFalse(vals.TryAdd(keyPresent, 2));
        }
    }
}