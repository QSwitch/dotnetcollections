﻿using SwitchCollectionExtensions.DictionaryExtensions;
using SwitchCollectionExtensions.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensionsTests.DictionaryExtensions
{
    [TestClass]
    public class PartitionExtensionsTests
    {
        private Dictionary<string, int> _testValues = new Dictionary<string, int>()
        {
            { "1", 1},
            { "2", 2},
            { "3", 3},
            { "4", 4},
            { "5", 5},
            { "6", 6},
            { "7", 7},
            { "8", 8},
            { "9", 9},
            { "10", 10},
        };

        [TestMethod()]
        public void TestPartition_ZeroSize_ReturnsWholeCollection()
        {
            List<Pair<string, int>> pairsFromDict = GetKeySortedPairsFromDictionary();
            List<Pair<string, int>> partitioned = _testValues.PartitionByKey(-1).First().ToList();

            AssertMatchingLists(pairsFromDict, partitioned);
        }

        private List<Pair<string, int>> GetKeySortedPairsFromDictionary()
        {
            List<string> keys = _testValues.Keys.ToList();

            keys.Sort();

            List<Pair<string, int>> pairsFromDict =
                keys.Select(key => new Pair<string, int>(key, _testValues[key])).ToList();
            return pairsFromDict;
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsWholeCollection()
        {
            List<Pair<string, int>> pairsFromDict = GetKeySortedPairsFromDictionary();
            List<Pair<string, int>> partitioned = _testValues.PartitionByKey(-1).First().ToList();

            AssertMatchingLists(pairsFromDict, partitioned);
        }

        private void AssertMatchingLists(List<Pair<string, int>> expected, List<Pair<string, int>> actual)
        {
            Assert.AreEqual(expected.Count, actual.Count);

            foreach (int index in Enumerable.Range(0, expected.Count))
            {
                Assert.AreEqual(expected[index].First, actual[index].First);
                Assert.AreEqual(expected[index].Second, actual[index].Second);
            }
        }

        [TestMethod()]
        public void TestPartition_NegativeSize_ReturnsOnlyCollection()
        {
            Assert.AreEqual(1, _testValues.PartitionByKey(-1).Take(2).Count());
        }

        [TestMethod()]
        public void TestPartition_SingleSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(1);
        }

        private void AssertEvenDivisionPartitionResults(int partitionSize)
        {
            IEnumerable<IEnumerable<Pair<string, int>>> partitioned = _testValues.PartitionByKey(partitionSize);

            Assert.AreEqual(_testValues.Count / partitionSize, partitioned.Count());

            foreach (IEnumerable<Pair<string, int>> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        [TestMethod()]
        public void TestPartition_ExactDivisorSize_ReturnsNCollections()
        {
            AssertEvenDivisionPartitionResults(2);
        }

        [TestMethod()]
        public void TestPartition_UnevenDivisorSize_ReturnsOnlyFullPartitions()
        {
            int partitionSize = 3;
            IEnumerable<IEnumerable<Pair<string, int>>> partitioned = _testValues.PartitionByKey(partitionSize);
            int expectedCount = _testValues.Count / partitionSize;

            Assert.AreEqual(expectedCount, partitioned.Count());

            foreach (IEnumerable<Pair<string, int>> partition in partitioned)
            {
                Assert.AreEqual(partitionSize, partition.Count());
            }
        }

        // TODO: Is there a reasonable way to construct a padded partition function?  One possibility is to provide a
        // function that generates the padding pairs.
        //[TestMethod()]
        //public void TestPartitionWithPad_UnevenDivisorSize_ReturnsPaddedLastCollection()
        //{
        //    int partitionSize = 3;
        //    Pair<string, int> pad = new Pair<string, int>("-1", -1);
        //    IEnumerable<IEnumerable<Pair<string, int>>> partitioned = _testValues.PartitionByKey(
        //        partitionSize,
        //        partitionSize,
        //        pad);
        //    int expectedCount = 1 + _testValues.Count / partitionSize;

        //    Assert.AreEqual(expectedCount, partitioned.Count());

        //    foreach (IEnumerable<Pair<string, int>> partition in partitioned)
        //    {
        //        Assert.AreEqual(partitionSize, partition.Count());
        //    }

        //    Assert.AreEqual(
        //        _testValues.Count % partitionSize,
        //        partitioned.Last().TakeWhile(pair => pair.First != pad.First && pair.Second != pad.Second).Count());
        //}

        [TestMethod]
        public void TestPartition_SingleSize_OrderedKeys()
        {
            IEnumerable<IEnumerable<Pair<string, int>>> partitioned = _testValues.PartitionByKey(2);
            List<string> keys = _testValues.Keys.ToList();

            keys.Sort();

            foreach (IEnumerable<Pair<string, int>> partition in partitioned)
            {
                int partitionLength = partition.Count();
                string startKey = partition.First().First;
                IEnumerable<string> expectedKeys = keys.SkipWhile(key => !key.Equals(startKey));

                foreach (int index in Enumerable.Range(0, partitionLength))
                {
                    Assert.AreEqual(expectedKeys.ElementAt(index), partition.ElementAt(index).First);
                }
            }
        }

        [TestMethod]
        public void TestPartitionBy_ListOfIntsEndsWithPartitionOfOneWithGreaterThanPartition_ListsSplitByKeyValue()
        {
            Dictionary<string, int> values = new Dictionary<string, int>()
            {
                {"1", 1},
                {"2", 1},
                {"4", 1},
                {"5", 2},
                {"6", 2},
                {"3", 2},
                {"7", 3},
                {"8", 3},
                {"9", 1},
            };
            IEnumerable<IEnumerable<Pair<string, int>>> partitioned = values.PartitionBy((a) => "5".CompareTo(a) > 0);

            Assert.AreEqual(4, partitioned.Count());
            Assert.AreEqual(3, partitioned.ElementAt(0).Count());
            Assert.AreEqual(2, partitioned.ElementAt(1).Count());
            Assert.AreEqual(1, partitioned.ElementAt(2).Count());
            Assert.AreEqual(3, partitioned.ElementAt(3).Count());
        }

        [TestMethod]
        public void TestSortedPartitionBy_ListOfIntsEndsWithPartitionOfOneWithGreaterThanPartition_ListsSplitByKeyValue()
        {
            Dictionary<string, int> values = new Dictionary<string, int>()
            {
                {"1", 1},
                {"2", 1},
                {"4", 1},
                {"5", 2},
                {"6", 2},
                {"3", 2},
                {"7", 3},
                {"8", 3},
                {"9", 1},
            };
            IEnumerable<IEnumerable<Pair<string, int>>> partitioned =
                values.SortedPartitionBy((a) => "5".CompareTo(a) > 0);

            Assert.AreEqual(2, partitioned.Count());
            Assert.AreEqual(4, partitioned.First().Count());
            Assert.AreEqual(5, partitioned.Last().Count());
        }
    }
}