# README #

## _SwitchCollectionExtensions_ ##

* Summary

This package includes extensions for .NET collections.  The extensions are not already included in the System.Linq package, but are useful.  Some were inspired by functions for working with collections in functional languages.  These include functions such as zip (join two collections in pairs element by element without having any sort of grouping selector in mind), partition (split a collection into sub-collections of a given size, optionally skipping elements or creating overlapping partitions) and interleave (create a new collection from a given collection and an item or items to put between elements).  Otheres are simply useful, such as being able to execute ForEach on an IEnumerable or Select only on items that meet a given criteria (i.e. putting Select and Where together).

* Version

1.0.0

### How do I get set up? ###
Nothing special is required.  Just install the package (SwitchCollectionExtensions) through NuGet and start using it.  It does use some of the System.Linq package internally.

### Contribution guidelines ###

I'm happy to look at contributions.  This package is going to be growing over time.  Often it'll grow when I come across something I'd like to be able to do with collections in .NET that is not supported in the .NET libraries.  Other times it'll be when I come across something particularly nifty in a functional language that .NET doesn't have.  Functions like Zip fall into both these categories.

Any actual code submissions I'll review.  No submissions will be considered if there isn't a desription of the functionality, the scenarios covered and accompanying tests to demonstrate that the changes work and aren't going to be throwing unjustified exceptions.

### Who do I talk to? ###

I havent' currently got any specific communication channels set up.  I will most likely end up putting up an email address that I don't mind receving almost 100% spam along with instructions on what to put in the subject line so that the mailbox rules don't delete your message.