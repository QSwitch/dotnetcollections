﻿using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensions.IEnumerableExtensions
{
    public static class AppendExtensions
    {
        /// <summary>
        /// Append one or more collections to the calling collection.  Appending constructs a collection with the items 
        /// of the nth collection appearing immediately after the items of the previous one.
        /// 
        /// NOTE: This method will work for non-List enumerables, such as HashSets.  The result will not follow the
        /// rules of those collections, though - duplicates are allowed since the result is actually just a sequence, 
        /// not the same type as the input.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="coll"></param>
        /// <param name="toAppend"></param>
        /// <param name="moreColls"></param>
        /// <returns></returns>
        public static IEnumerable<T> Append<T>(
            this IEnumerable<T> coll,
            params IEnumerable<T>[] moreColls)
        {
            foreach (T val in coll)
            {
                yield return val;
            }

            foreach (IEnumerable<T> anotherColl in moreColls)
            {
                if (null == anotherColl || !anotherColl.Any())
                {
                    continue;
                }

                foreach (T val in anotherColl)
                {
                    yield return val;
                }
            }
        }
    }
}
