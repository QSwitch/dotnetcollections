﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensions.IEnumerableExtensions
{
    public static class PartitionExtensions
    {
        /// <summary>
        /// From an input collection, take subsets of size <code>partitionSize</code>. If it is not possible to have
        /// all subsets equal to <code>partitionSize</code> then ignore the input entries that don't fit.  So if a
        /// collection of 10 items is to be broken into subsets of size 3, then the last element of the input will not
        /// appear in any output subset.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="coll"></param>
        /// <param name="partitionSize"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> coll, int partitionSize)
        {
            if (partitionSize <= 0 || !coll.Any())
            {
                yield return coll;
            }
            else
            {
                IEnumerable<IEnumerable<T>> partitions = coll.Partition(partitionSize, partitionSize);

                foreach (IEnumerable<T> partition in partitions)
                {
                    yield return partition;
                }
            }
        }

        /// <summary>
        /// From an input collection, take subsets of size <code>partitionSize</code>.  Each subset starting point in
        /// the input collection is determined by adding the <code>stepSize</code> to the starting point of the previous
        /// subset. If it is not possible to have all subsets equal to <code>partitionSize</code> then ignore the input 
        /// entries that don't fit.  So if a collection of 10 items is to be broken into subsets of size 3 with a
        /// <code>stepSize</code> of 2, then the output will be subsets [a b c], [c d e], [e f g], [g h i] with no
        /// subset containing item j.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="coll"></param>
        /// <param name="partitionSize"></param>
        /// <param name="stepSize"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> coll, int partitionSize, int stepSize)
        {
            if (partitionSize <= 0 || !coll.Any())
            {
                yield return coll;
            }
            else
            {
                if (partitionSize <= 0 || !coll.Any())
                {
                    yield return coll;
                }
                else
                {
                    if (stepSize < 1)
                    {
                        stepSize = 1;
                    }

                    int pos = 0;
                    int lastCount = partitionSize;

                    while (lastCount == partitionSize)
                    {
                        IEnumerable<T> partition = GetPartition(coll, pos, partitionSize);

                        lastCount = partition.Count();

                        if (lastCount == partitionSize)
                        {
                            yield return partition;
                        }

                        pos += stepSize;
                    }
                }
            }
        }

        /// <summary>
        /// From an input collection, take subsets of size <code>partitionSize</code>.  Each subset starting point in
        /// the input collection is determined by adding the <code>stepSize</code> to the starting point of the previous
        /// subset. If it is not possible to have all subsets equal to <code>partitionSize</code> then use the pad item
        /// to complete the subset.  So if a collection of 10 items is to be broken into subsets of size 3 with a
        /// <code>stepSize</code> of 2, then the output will be subsets [a b c], [c d e], [e f g], [g h i] [i j pad].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="coll"></param>
        /// <param name="partitionSize"></param>
        /// <param name="stepSize"></param>
        /// <param name="pad"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Partition<T>(
            this IEnumerable<T> coll,
            int partitionSize,
            int stepSize,
            T pad)
        {
            if (partitionSize <= 0 || !coll.Any())
            {
                yield return coll;
            }
            else
            {
                stepSize = Math.Max(stepSize, 1);

                int pos = 0;
                int lastCount = partitionSize;

                while (lastCount == partitionSize)
                {
                    IEnumerable<T> partition = GetPartition(coll, pos, partitionSize);

                    lastCount = partition.Count();

                    if (lastCount == partitionSize)
                    {
                        yield return partition;
                    }
                    else
                    {
                        int repeatCount = partitionSize - partition.Count();

                        yield return partition.Append(Enumerable.Repeat(pad, repeatCount));
                    }

                    pos += stepSize;
                }
            }
        }

        private static IEnumerable<T> GetPartition<T>(IEnumerable<T> coll, int pos, int partitionSize)
        {
            return coll.Skip(pos).Take(partitionSize);
        }

        public static IEnumerable<T> EveryNth<T>(this IEnumerable<T> coll, int nth)
        {
            IEnumerable<T> result = coll.Partition(1, nth).SelectMany(entry => entry);

            foreach (T val in result)
            {
                yield return val;
            }
        }
    }
}
