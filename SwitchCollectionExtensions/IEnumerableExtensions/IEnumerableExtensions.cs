﻿using System;
using System.Collections.Generic;

namespace SwitchCollectionExtensions.IEnumerableExtensions
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Because .NET generally has static types, a collection can contain collections only if all it contains
        /// are collections.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="coll"></param>
        /// <returns></returns>
        public static IEnumerable<U> Flatten<T, U>(this IEnumerable<T> coll) where T : IEnumerable<U>
        {
            List<U> flattened = new List<U>();

            foreach (T val in coll)
            {
                flattened.AddRange(val);

            }

            return flattened;
        }

        public static void ForEach<T>(this IEnumerable<T> coll, Action<T> action)
        {
            foreach (T val in coll)
            {
                action(val);
            }
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> coll)
        {
            HashSet<T> newHashSet = new HashSet<T>();

            foreach (T val in coll)
            {
                newHashSet.Add(val);
            }

            return newHashSet;
        }

        public static IEnumerable<U> SelectWhere<T, U>(
        this IEnumerable<T> coll,
        Predicate<T> filter,
        Func<T, U> selector)
        {
            foreach (T element in coll)
            {
                if (filter(element))
                {
                    yield return selector(element);
                }
            };
        }

        public static IEnumerable<U> SelectDistinctWhere<T, U>(
        this IEnumerable<T> coll,
        Predicate<T> filter,
        Func<T, U> selector) where U : IComparable<U>
        {
            HashSet<U> alreadySeen = new HashSet<U>();

            foreach (T element in coll)
            {
                if (filter(element))
                {
                    U selectedValue = selector(element);

                    if (!alreadySeen.Contains(selectedValue))
                    {
                        alreadySeen.Add(selectedValue);
                        yield return selectedValue;
                    }
                }
            };
        }
    }
}
