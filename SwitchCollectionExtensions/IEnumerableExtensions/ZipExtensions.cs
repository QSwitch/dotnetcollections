﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensions.IEnumerableExtensions
{
    public static class ZipExtensions
    {
        public static IEnumerable<List<T>> Zip<T>(this IEnumerable<T> lhs, params IEnumerable<T>[] rhs)
        {
            if (null == rhs || !rhs.Any() || !rhs.Any(anotherColl => anotherColl.Any()) || !lhs.Any())
            {
                yield return new List<T>();
            }
            else
            {
                foreach (List<T> zip in GetNextZip(lhs, rhs, (lhsHasVal, rhsHasVal) => lhsHasVal && rhsHasVal))
                {
                    yield return zip;
                }
            }
        }

        public static IEnumerable<List<T>> ZipAny<T>(this IEnumerable<T> lhs, params IEnumerable<T>[] rhs)
        {
            if (null == rhs || !rhs.Any())
            {
                foreach (T val in lhs)
                {
                    yield return new List<T>() { val };
                }
            }
            else if (!rhs.Any(anotherColl => anotherColl.Any()))
            {
                foreach (T val in lhs)
                {
                    List<T> result = new List<T>() { val, };
                    result.AddRange(Enumerable.Repeat(default(T), rhs.Length));

                    yield return result;
                }
            }
            else
            {
                foreach (List<T> zip in GetNextZip(lhs, rhs, (lhsHasVal, rhsHasVal) => lhsHasVal || rhsHasVal))
                {
                    yield return zip;
                }
            }
        }

        private static IEnumerable<List<T>> GetNextZip<T>(
            IEnumerable<T> lhs,
            IEnumerable<T>[] rhs,
            Func<bool, bool, bool> shouldYieldCollection)
        {
            IEnumerator<T> lhsEnumerator = lhs.GetEnumerator();
            List<IEnumerator<T>> rhsEnumerators =
                  rhs.Where(anotherColl => null != anotherColl)
                      .Select(anotherColl => anotherColl.GetEnumerator())
                      .ToList();

            try
            {
                bool lhsHasValue = lhsEnumerator.MoveNext();
                bool rhsHasValue = rhsEnumerators.All(anotherCollEnumerator => anotherCollEnumerator.MoveNext());

                while (shouldYieldCollection(lhsHasValue, rhsHasValue))
                {
                    List<T> nextZip = new List<T>()
                    {
                        lhsEnumerator.Current,
                    };

                    nextZip.AddRange(rhsEnumerators.Select(anotherCollEnumerator => anotherCollEnumerator.Current));
                    lhsHasValue = lhsEnumerator.MoveNext();
                    rhsHasValue = rhsEnumerators.All(anotherCollEnumerator => anotherCollEnumerator.MoveNext());

                    yield return nextZip;
                }
            }
            finally
            {
                lhsEnumerator.Dispose();
                rhsEnumerators.ForEach(enumerator => enumerator.Dispose());
            }
        }
    }
}
