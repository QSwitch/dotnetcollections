﻿using System;

namespace SwitchCollectionExtensions.Types
{
    /// <summary>
    /// A type to hold the possibility of a value.  The DefaultOption has three states:
    /// 1) A value is set that is not a default value.  The DefaultOption HasValue = true and IsDefault = false.
    /// 2) A default value is set explicitly.  The DefaultOption Value is that default, HasValue = true and 
    ///		IsDefault = true.
    /// 3) A default value is set because no other value is.  The DefaultOption Value throws an exception, 
    ///		HasValue = false and IsDefault = false.
    /// 
    /// If an external default value is provided then the internal value is set to that and the DefaultOption always at
    /// least has that value.
    /// 
    /// The Predicate functions are functions that identify default values.  So a Predicate that tests tVal &gt; 0 is
    /// marking as default any tVal that is greater than 0.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Option<T>
    {
        private T _internalValue;
        private bool _initialised = false;

        public T Value
        {
            get
            {
                if (!HasValue)
                {
                    throw new InvalidOperationException("The Option has no value.");
                }

                return _internalValue;
            }
            private set
            {
                if (!_initialised)
                {
                    _internalValue = value;
                    HasValue = !_isDefaultForType(_internalValue);
                    _initialised = true;
                }
            }
        }

        private bool _hasValue;

        public bool HasValue
        {
            get
            {
                return _hasValue;
            }
            private set { _hasValue = value; }
        }

        protected Predicate<T> _isDefaultForType;
        protected Predicate<T> _isDefaultForValueType;
        protected Predicate<T> _isDefaultForRefType;

        /// <summary>
        /// IsDefaultForType and the methods it calls require an input value otherwise any replacement functions passed 
        /// in are limited in the types of comparison they could do because they'd possibly not be able to refer to the 
        /// value passed in, for instance if the function used is not an anonymous function, but one that is a member of
        /// the calling class (which then wouldn't have direct access to the value set in the Option at the time of the
        /// Option's construction).
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        protected virtual bool IsDefaultForType(T val)
        {
            Type tType = typeof(T);

            if (tType.IsValueType)
            {
                return _isDefaultForValueType(val);
            }
            else if (tType.IsClass || tType.IsInterface)
            {
                return _isDefaultForRefType(val);
            }

            return true;
        }

        protected virtual bool IsDefaultValueType(T val)
        {
            return default(T).Equals(val);
        }

        protected virtual bool IsDefaultRefType(T val)
        {
            return null == val;
        }

        public Option(T val)
        : this(val, null)
        {
        }

        public Option(T val, Predicate<T> isDefaultValue)
        {
            SetIsDefaultFunction(isDefaultValue);
            Value = val;
        }

        private void SetIsDefaultFunction(Predicate<T> isDefaultValue)
        {
            if (null == isDefaultValue)
            {
                SetDefaultIsDefaultFunctions();
            }
            else
            {
                _isDefaultForType = isDefaultValue;
            }
        }

        private void SetDefaultIsDefaultFunctions()
        {
            _isDefaultForType = IsDefaultForType;
            _isDefaultForRefType = IsDefaultRefType;
            _isDefaultForValueType = IsDefaultValueType;
        }

        //public Option(Predicate<T> isDefaultValue)
        //{
        //    SetIsDefaultFunction(isDefaultValue);
        //    _internalValue = default(T);
        //    HasValue = !_isDefaultForType(_internalValue);
        //}
    }
}
