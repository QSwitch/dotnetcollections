﻿using System.Collections.Generic;

namespace SwitchCollectionExtensions.Types
{
    public class Pair<T, U>
    {
        public T First { get; set; }
        public U Second { get; set; }

        public Pair(T fst, U scd)
        {
            First = fst;
            Second = scd;
        }

        public Pair(KeyValuePair<T, U> keyValPair)
        {
            First = keyValPair.Key;
            Second = keyValPair.Value;
        }
    }
}