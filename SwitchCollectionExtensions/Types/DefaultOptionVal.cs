﻿using System;

namespace CollectionExtensions.Types
{
    public class DefaultOptionVal<T> : DefaultOption<T> where T : struct
    {
        public DefaultOptionVal(T defaultVal)
	    : base(defaultVal)
        {
        }

        public DefaultOptionVal(T defaultVal, Func<bool> isDefaultValue)
	    : base(defaultVal, isDefaultValue)
        {
        }

        public DefaultOptionVal(T defaultVal, Func<bool> isDefaultValue, Func<bool> isDefaultForType)
	    : base (defaultVal, isDefaultValue, isDefaultForType)
        {
        }

        public DefaultOptionVal(T val, T defaultVal)
	    : base(val, defaultVal)
        {
        }

        public DefaultOptionVal(T val, T defaultVal, Func<bool> isDefaultValue, Func<bool> isDefaultForType)
	    : base(val, defaultVal, isDefaultValue, isDefaultForType)
        {
        }

        protected override bool IsDefaultForType()
        {
            return default(T).Equals(Value);
        }

        public override bool IsDefaultValue()
        {
            return DefaultValue.Equals(Value);
        }
    }
}
