﻿using System;

namespace CollectionExtensions.Types
{
    public class DefaultOptionRef<T> : DefaultOption<T> where T : class
    {
        public DefaultOptionRef(T defaultRef)
	    : base(defaultRef)
        {
        }

        public DefaultOptionRef(T defaultRef, Func<bool> isDefaultRefue)
	    : base(defaultRef, isDefaultRefue)
        {
        }

        public DefaultOptionRef(T defaultRef, Func<bool> isDefaultRefue, Func<bool> isDefaultForType)
	    : base (defaultRef, isDefaultRefue, isDefaultForType)
        {
        }

        public DefaultOptionRef(T val, T defaultRef)
	    : base(val, defaultRef)
        {
        }

        public DefaultOptionRef(T val, T defaultRef, Func<bool> isDefaultRefue, Func<bool> isDefaultForType)
	    : base(val, defaultRef, isDefaultRefue, isDefaultForType)
        {
        }

        protected override bool IsDefaultForType()
        {
            return null == Value;
        }

        public override bool IsDefaultValue()
        {
            return !IsDefaultForType() && DefaultValue.Equals(Value);
        }
    }
}
