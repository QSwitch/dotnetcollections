﻿using System;

namespace CollectionExtensions.Types
{
    public abstract class DefaultOption<T>
    {
        public T DefaultValue { get; protected set; }

        private T _internalValue;
        private bool _initialised = false;

        public T Value
        {
            get
            {
                if (!HasValue)
                {
                    return DefaultValue;
                }

                return _internalValue;
            }
            set
            {
                if (!_initialised)
                {
                    _internalValue = value;
                    HasValue = _isDefaultForType();
                }
            }
        }

        private bool _hasValue;

        public bool HasValue
        {
            get
            {
                return _hasValue;
            }
            private set { _hasValue = value; }
        }

        protected Func<bool> _isDefaultForType;
        protected Func<bool> _isDefaultValue;

        protected abstract bool IsDefaultForType();
        public abstract bool IsDefaultValue();

        public DefaultOption(T defaultVal)
        {
            _internalValue = defaultVal;
            DefaultValue = defaultVal;
            _isDefaultForType = IsDefaultForType;
            _isDefaultValue = IsDefaultValue;
        }

        public DefaultOption(T defaultVal, Func<bool> isDefaultValue)
        {
            _internalValue = defaultVal;
            DefaultValue = defaultVal;
            _isDefaultForType = IsDefaultForType;
            _isDefaultValue = isDefaultValue;
        }

        public DefaultOption(T defaultVal, Func<bool> isDefaultValue, Func<bool> isDefaultForType)
        {
            _internalValue = defaultVal;
            DefaultValue = defaultVal;
            _isDefaultForType = isDefaultForType;
            _isDefaultValue = isDefaultValue;
        }

        public DefaultOption(T val, T defaultVal)
        {
            Value = val;
            DefaultValue = defaultVal;
            _isDefaultForType = IsDefaultForType;
            _isDefaultValue = IsDefaultValue;
        }

        public DefaultOption(T val, T defaultVal, Func<bool> isDefaultValue, Func<bool> isDefaultForType)
        {
            Value = val;
            DefaultValue = defaultVal;
            _isDefaultForType = isDefaultForType;
            _isDefaultValue = isDefaultValue;
        }
    }
}
