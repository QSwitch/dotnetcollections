﻿using System;
using System.Collections.Generic;

namespace SwitchCollectionExtensions.CollectionGenerators
{
    public static class GenerateEnumerable
    {
        public static IEnumerable<T> Iterate<T>(this T init, Func<T, T> action)
        {
            if (null == action)
            {
                yield break;
            }

            T currentValue = init;

            while (true)
            {
                yield return currentValue;
                currentValue = action(currentValue);
            }
        }
    }
}
