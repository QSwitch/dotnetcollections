﻿using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensions.ListExtensions
{
    public static class ListExtensions
    {
        public static IEnumerable<T> Interleave<T>(this List<T> lhs, List<T> rhs)
        {
            IEnumerator<T> lhsEnumerator = lhs.GetEnumerator();

            if (null == rhs || !rhs.Any())
            {
                while (lhsEnumerator.MoveNext())
                {
                    yield return lhsEnumerator.Current;
                }
            }
            else
            {
                int srcIndex = 2;
                IEnumerator<T> rhsEnumerator = rhs.GetEnumerator();
                List<IEnumerator<T>> srcPair = new List<IEnumerator<T>>() { lhsEnumerator, rhsEnumerator };
                List<bool> srcCollExhausted = new List<bool>() { !lhs.Any(), !rhs.Any() };

                while (srcCollExhausted.Any(exhausted => !exhausted))
                {
                    srcIndex = srcIndex % 2;

                    if (srcCollExhausted[srcIndex])
                    {
                        srcIndex = (srcIndex + 1) % 2;
                    }

                    IEnumerator<T> srcColl = srcPair[srcIndex];

                    if (srcColl.MoveNext())
                    {
                        yield return srcColl.Current;
                    }
                    else
                    {
                        srcCollExhausted[srcIndex] = true;
                    }

                    ++srcIndex;
                }
            }
        }

        public static IEnumerable<T> Interleave<T>(this List<T> target, T val)
        {
            IEnumerator<T> targetEnumerator = target.GetEnumerator();
            bool hasNext = targetEnumerator.MoveNext();

            while (hasNext)
            {
                yield return targetEnumerator.Current;
                hasNext = targetEnumerator.MoveNext();

                if (hasNext)
                {
                    yield return val;
                }
            }
        }
    }
}
