﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensions.ListExtensions
{
    public static class PartitionExtensions
    {
        public static IEnumerable<List<T>> PartitionBy<T, U>(
            this List<T> coll,
            Func<T, U> partitionFunction)
        {
            if (null == partitionFunction || !coll.Any())
            {
                yield return coll;
            }
            else
            {
                using (IEnumerator<T> collEnumerator = coll.GetEnumerator())
                {
                    collEnumerator.MoveNext();

                    List<T> partition = new List<T>();
                    T current = collEnumerator.Current;
                    U currentPartitionVal = partitionFunction(current);
                    U nextPartitionVal = currentPartitionVal;

                    partition.Add(current);

                    while (collEnumerator.MoveNext())
                    {
                        current = collEnumerator.Current;
                        currentPartitionVal = nextPartitionVal;
                        nextPartitionVal = partitionFunction(current);

                        if (!ValuesMatch(currentPartitionVal, nextPartitionVal))
                        {
                            yield return partition;
                            partition = new List<T>() { current };
                        }
                        else
                        {
                            partition.Add(current);
                        }
                    }

                    yield return partition;
                }
            }
        }

        private static bool ValuesMatch<T>(T litmus, T testVal)
        {
            return EqualityComparer<T>.Default.Equals(litmus, testVal);
        }
    }
}
