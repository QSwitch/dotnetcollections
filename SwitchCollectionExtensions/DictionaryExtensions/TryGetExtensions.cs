﻿using SwitchCollectionExtensions.Types;
using System;
using System.Collections.Generic;

namespace SwitchCollectionExtensions.DictionaryExtensions
{
    public static class TryGetExtensions
    {
        public static Option<U> TryGet<T, U>(this Dictionary<T, U> coll, T key)
        {
            if (coll.ContainsKey(key))
            {
                return new Option<U>(coll[key]);
            }

            return new Option<U>(default(U), (val) => true);
        }

        public static Option<U> TryGet<T, U>(this Dictionary<T, U> coll, T key, U defaultVal)
        {
            if (coll.ContainsKey(key))
            {
                U val = coll[key];

                return new Option<U>(val, (checkVal) => IsDefaultValue(checkVal, defaultVal));
            }

            return new Option<U>(defaultVal, (checkVal) => IsDefaultValue(checkVal, defaultVal));
        }

        private static bool IsDefaultValue<T>(T checkValue, T defaultValue)
        {
            Type tType = typeof(T);

            if (tType.IsValueType)
            {
                return IsDefaultValValue(checkValue, defaultValue);
            }
            else if (tType.IsClass || tType.IsInterface)
            {
                return IsDefaultRefValue(checkValue, defaultValue);
            }
            else
            {
                return true;
            }
        }

        private static bool IsDefaultRefValue<T>(T checkValue, T defaultValue)
        {
            return null == (object)checkValue;
        }

        private static bool IsDefaultValValue<T>(T checkValue, T defaultValue)
        {
            return false;
        }
    }
}
