﻿using System.Collections.Generic;

namespace SwitchCollectionExtensions.DictionaryExtensions
{
    public static class AddExtensions
    {
        public static bool TryAdd<T, U>(this Dictionary<T, U> coll, T key, U val)
        {
            if (null == key || coll.ContainsKey(key))
            {
                return false;
            }

            coll.Add(key, val);
            return true;
        }
    }
}
