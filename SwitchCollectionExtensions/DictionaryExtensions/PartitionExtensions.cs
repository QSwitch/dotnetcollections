﻿using SwitchCollectionExtensions.IEnumerableExtensions;
using SwitchCollectionExtensions.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SwitchCollectionExtensions.DictionaryExtensions
{
    public static class PartitionExtensions
    {
        public static IEnumerable<IEnumerable<Pair<T, U>>> PartitionByKey<T, U>(
            this Dictionary<T, U> coll,
            int partitionSize)
        {
            List<T> keys = coll.Keys.ToList();

            keys.Sort();

            foreach (IEnumerable<T> keyPartition in keys.Partition(partitionSize))
            {
                yield return keyPartition.Select(key => new Pair<T, U>(key, coll[key]));
            }
        }

        public static IEnumerable<IEnumerable<Pair<T, U>>> PartitionByKey<T, U>(
            this Dictionary<T, U> coll,
            int partitionSize,
            int stepSize)
        {
            if (partitionSize <= 0 || !coll.Any())
            {
                yield return coll.Select(keyAndValue => new Pair<T, U>(keyAndValue.Key, keyAndValue.Value));
            }
            else
            {
                List<T> keys = coll.Keys.ToList();

                keys.Sort();

                foreach (IEnumerable<T> keyPartition in keys.Partition(partitionSize, stepSize))
                {
                    yield return keyPartition.Select(key => new Pair<T, U>(key, coll[key]));
                }
            }
        }

        public static IEnumerable<IEnumerable<Pair<T, U>>> PartitionBy<T, U, V>(
            this Dictionary<T, U> coll,
            Func<T, V> partitionFunction)
        {
            foreach (IEnumerable<Pair<T, U>> partition in PartitionBy(coll, partitionFunction, GetKeys))
            {
                yield return partition;
            }
        }

        private static IEnumerable<IEnumerable<Pair<T, U>>> PartitionBy<T, U, V>(
            Dictionary<T, U> coll,
            Func<T, V> partitionFunction,
            Func<Dictionary<T, U>, IEnumerable<T>> getKeys)
        {
            if (null == partitionFunction || !coll.Any())
            {
                yield return new List<Pair<T, U>>();
            }
            else
            {
                IEnumerable<T> keys = getKeys(coll);

                foreach (List<Pair<T, U>> partition in PartitionBy(coll, partitionFunction, keys))
                {
                    yield return partition;
                }
            }
        }

        private static IEnumerable<T> GetKeys<T, U>(Dictionary<T, U> coll)
        {
            return coll.Keys;
        }

        private static IEnumerable<List<Pair<T, U>>> PartitionBy<T, U, V>(
            Dictionary<T, U> coll,
            Func<T, V> partitionFunction,
            IEnumerable<T> keys)
        {
            using (IEnumerator<T> collEnumerator = keys.GetEnumerator())
            {
                collEnumerator.MoveNext();

                List<Pair<T, U>> partition = new List<Pair<T, U>>();
                T currentKey = collEnumerator.Current;
                Pair<T, U> current = new Pair<T, U>(currentKey, coll[currentKey]);
                V currentPartitionVal = partitionFunction(currentKey);
                V nextPartitionVal = currentPartitionVal;

                partition.Add(current);

                while (collEnumerator.MoveNext())
                {
                    currentKey = collEnumerator.Current;
                    current = new Pair<T, U>(currentKey, coll[currentKey]);
                    currentPartitionVal = nextPartitionVal;
                    nextPartitionVal = partitionFunction(currentKey);

                    if (!ValuesMatch(currentPartitionVal, nextPartitionVal))
                    {
                        yield return partition;
                        partition = new List<Pair<T, U>>() { current };
                    }
                    else
                    {
                        partition.Add(current);
                    }
                }

                yield return partition;
            }
        }

        private static bool ValuesMatch<T>(T litmus, T testVal)
        {
            return EqualityComparer<T>.Default.Equals(litmus, testVal);
        }

        public static IEnumerable<IEnumerable<Pair<T, U>>> SortedPartitionBy<T, U, V>(
            this Dictionary<T, U> coll,
            Func<T, V> partitionFunction)
        {
            foreach (IEnumerable<Pair<T, U>> partition in PartitionBy(coll, partitionFunction, GetSortedKeys))
            {
                yield return partition;
            }
        }

        private static IEnumerable<T> GetSortedKeys<T, U>(Dictionary<T, U> coll)
        {
            List<T> keys = coll.Keys.ToList();

            keys.Sort();
            return keys;
        }
    }
}
